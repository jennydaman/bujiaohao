import Vue from 'vue';
import FastClick from 'fastclick';
import Router from 'vue-router';
import App from './App';
import Home from './components/Home';
import Display from './components/Display';
import Controller from './components/Controller';
import NotFoundComponent from './components/NotFoundComponent';

import { AjaxPlugin, numberPad } from 'vux';

Vue.use(Router);
Vue.use(AjaxPlugin);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/display',
      component: Display
    },
    {
      path: '/controller',
      component: Controller
    },
    {
      path: '*',
      component: NotFoundComponent
    }
  ]
});

FastClick.attach(document.body);

Vue.config.productionTip = false;

Vue.mixin({
  methods: {
    numberPad: numberPad,
    reduceNumber (num) {
      if (num < 101)
        return num;

      let remainder = num % 100;
      if (remainder === 0)
        return 100;
      return remainder;
    },
    prettyNumber (num) {
      num = this.reduceNumber(num);
      return this.numberPad(num, 3);
    }
  }
});

/* eslint-disable no-new */
new Vue({
  router,
  render: h => h(App)
}).$mount('#app-box');
