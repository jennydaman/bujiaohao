const express = require('express');
const SSE = require('express-sse');
const history = require('connect-history-api-fallback');
const systemd = require('sd-notify');
const app = express();

const sse = new SSE(['mian']);

app.get('/sse', sse.init);

setInterval(() => {
  sse.send('lubdub', 'heartbeat');
}, 5000);

app.get('/controller/rest/:action', (req, res) => {
  if (['order', 'remove'].indexOf(req.params.action) === -1) {
    output(req, 404);
    res.status(404).send('endpoint does not exist');
    return;
  }
  if (!req.query.number || !req.query.time) {
    output(req, 400);
    res.status(400).send('required GET parameters "number" and "time" missing')
    return;
  }
  const number = parseInt(req.query.number);
  if (!number) {
    output(req, 400);
    res.status(400).send('bad order number');
    return;
  }
  const time = new Date(req.query.time);
  if (time.toString() === 'Invalid Date') {
    output(req, 400);
    res.status(400).send('invalid date');
    return;
  }
  sse.send({number: number, time: time}, req.params.action);
  res.sendStatus(200);
  output(req, 200);
});

app.get('/health', (req, res) => {
  res.sendStatus(200);
});

app.use(history());
app.use('/', express.static('dist'));


const port = process.env.PORT || 8001;
app.listen(port, 'localhost', () => {
  console.log(`express server is online at http://localhost:${port}/`);
  systemd.ready();
});


function output(req, result) {
  if (process.argv.length < 3 || !process.argv[2].includes(('-v')))
    return;
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DateTimeFormat
  const options = {
    year: 'numeric', month: 'numeric', day: 'numeric',
    hour: 'numeric', minute: 'numeric', second: 'numeric',
    hour12: false,
    timeZone: 'America/New_York'
  };
  const date = new Intl.DateTimeFormat('en-US', options).format(new Date());
  console.log(`[${date}] from ${req.ip} : ${req.originalUrl} - ${result}`);
}
